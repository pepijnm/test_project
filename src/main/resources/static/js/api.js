function Api(){
    var starturl = "//localhost:8080"
    this.crud = function crud(protocol, url, data =null, async= false, returnfunction= null) {//protocol=put,delete,post,get
        $.ajax({
                url: starturl + url,
                type: protocol,
                async:async,
                data: (protocol!="get")?JSON.stringify(data):null,
                headers: {
                    "Content-Type": "application/json"
                },
                dataType: 'json',//zorg wel dat je json terug stuurt. puur strings luken zo niet.
                error: function (error) {
                console.log("error");
                        if(returnfunction==null){return false;}
                        else{returnfunction(error.responseJSON);}
                    },
                success: function(data) {
                console.log("success");
                if(returnfunction==null){return true;}
                else{returnfunction(data);}
                      }
        })
  }
}