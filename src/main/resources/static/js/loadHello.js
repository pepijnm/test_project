let loadfunction = ()=>{};
$( document ).ready(function() {

    let api = new Api();
    api.crud("get", "/hello", {}, false, (data)=>{
    console.log(data);});
    $('#save').submit(function( event ) {
        event.preventDefault();//cancel default send
        api.crud("post", "/hello", {"id":$("#id").val(),"value":$("#name").val()}, false, (data)=>{
            $("#message").text(data.message);
            loadfunction();
            });
      });
        var datatable = $('#table_id').DataTable({
            select: true,
            data: [],
           columns: [{ data: 'id' },
             { data: 'value' },
             {data: "options", render: function ( data, type, row ) {
                                               // Combine the first and last names into a single table field
                                               return "<button onclick='remove(\""+row.id+"\")'>delete</button>";
                                           }}]});
      $("#reload").click(function() {
        loadfunction();
                 });
      $("#remove").click(function() {
      remove($("#id").val());
                 });

      $("#update").click(function() {
        api.crud("put", "/hello/"+$("#id").val(), {"value":$("#name").val()}, false, (data)=>{
                    $("#message").text(data.message);
                    loadfunction();
                    });
                 });

loadfunction = function(){
        $("#message").text("");
        api.crud("get", "/hello", null, false, (data)=>{
                  console.log(JSON.stringify(data.data));
                  datatable.clear();
                datatable.rows.add(data.data);
                  datatable.draw();
                  });
}
});
function remove(id){
    let api = new Api();
    api.crud("delete", "/hello/"+id, {}, false, (data)=>{
      $("#message").text(data.message);
      loadfunction();
      });
}