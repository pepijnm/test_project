package com.example.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
public class HelloController {
    private HashMap<String, String> data;
    HelloController(){
        data = new HashMap<>();
    }

    @DeleteMapping(value = "/hello/{_id}")
    public String delete(@PathVariable String _id, HttpServletResponse response) {
        String id;
        try {
            id = _id;
            if(data.get(id)!= null){
                data.remove(id);
                response.setStatus( HttpServletResponse.SC_OK  );
                return "{\"message\":\"successful\"}";
            }
        } catch(Exception e){}

        response.setStatus( HttpServletResponse.SC_BAD_REQUEST );
        return "{\"message\":\"Data not correct\"}";
    }
    @GetMapping("/hello")//voor krijgen van gegevens
    public Map<String, Object> getAllData(@RequestParam(required = false) String search) {
        if(search==null){
            return convertToJson(data, "data", null);
        }
        else {
            HashMap<String, String> filterdata = new HashMap<>();
            data.forEach((i,s)->{
                if(s.contains(search)){
                    filterdata.put(i,s);
                }
            });
            return convertToJson(filterdata, "data", null);
        }
    }
    @GetMapping("/hello/{_id}")
    public String getData(@PathVariable String _id, HttpServletResponse response) {
        String id;
        try {
            id = _id;
            HashMap<String, String> result = new HashMap<>();
            if (data.get(id) != null) {
                result.put(id, data.get(id));
                return "{\"id\":"+id+",\"value\":\""+data.get(id)+"\"}";
            }
        } catch(Exception e){}
        response.setStatus( HttpServletResponse.SC_BAD_REQUEST );
        return"{\"message\":\"Could not find\"}";
    }

    @PutMapping("/hello/{_id}")
    public String update(@PathVariable String _id, @RequestBody Map<String, String> info, HttpServletResponse response) {
        String id;
        try {
            id = _id;
            if(data.get(id)!= null && info.get("value")!= null){
                data.put(id, info.get("value"));
                response.setStatus( HttpServletResponse.SC_OK  );
                return "{\"message\":\"successful\"}";
            }
        } catch(Exception e){}

        response.setStatus( HttpServletResponse.SC_BAD_REQUEST );
        return "{\"message\":\"Data not correct\"}";
    }
    @PostMapping(value = "/hello")
    public String add(@RequestBody Map<String, String> info, HttpServletResponse response){
        if(info.get("id") != null && info.get("value")!= null) {
            String id;
            try {
                id = info.get("id");
                if (data.get(id) == null) {
                    data.put(id, info.get("value"));
                    response.setStatus(HttpServletResponse.SC_OK);
                    return "{\"message\":\"successful\"}";
                }
            } catch(Exception e){}
        }
        response.setStatus( HttpServletResponse.SC_BAD_REQUEST );
        return "{\"message\":\"Data already correct\"}";
    }
    private HashMap<String, Object> convertToJson(HashMap<String,String> _data, String objectname, ArrayList<Object> startdata){//functie die {"data":[{"id":1,"value":"3"}]} bijvoorbeeld doet inplaats van {"3","4"}
        ArrayList<Object> array = new ArrayList<>();
        _data.forEach((i, s)->{
            array.add(new HashMap<String, Object>(){{put("id",i);put("value",s);}});
        });
        return new HashMap<String, Object>(){{put("data", array);}};
    }
}






//@RestController
//@RequestMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
//public class HelloController {
//    private HashMap<String, String> data;
//    HelloController(){
//        data = new HashMap<>();
//    }
//
//    @DeleteMapping(value = "/hello/{_id}")
//    public String delete(@PathVariable String _id, HttpServletResponse response) {
//        Integer id;
//        try {
//            id = Integer.parseInt(_id);
//            if(id>0  && data.get(id)!= null){
//                data.remove(id);
//                response.setStatus( HttpServletResponse.SC_OK  );
//                return "{\"message\":\"successful\"}";
//            }
//        } catch(Exception e){}
//
//        response.setStatus( HttpServletResponse.SC_BAD_REQUEST );
//        return "{\"message\":\"Data not correct\"}";
//    }
//    @GetMapping("/hello")//voor krijgen van gegevens
//    public Map<String, Object> getAllData(@RequestParam(required = false) String search) {
//        if(search==null){
//            return convertToJson(data, "data", null);
//        }
//        else {
//            HashMap<Integer, String> filterdata = new HashMap<>();
//            data.forEach((i,s)->{
//                if(s.contains(search)){
//                    filterdata.put(i,s);
//                }
//            });
//            return convertToJson(filterdata, "data", null);
//        }
//    }
//    @GetMapping("/hello/{_id}")
//    public String getData(@PathVariable String _id, HttpServletResponse response) {
//        Integer id;
//        try {
//            id = Integer.parseInt(_id);
//            HashMap<Integer, String> result = new HashMap<>();
//            if (id > 0 && data.get(id) != null) {
//                result.put(id, data.get(id));
//                return "{\"id\":"+id+",\"value\":\""+data.get(id)+"\"}";
//            }
//        } catch(Exception e){}
//        response.setStatus( HttpServletResponse.SC_BAD_REQUEST );
//        return"{\"message\":\"Could not find\"}";
//    }
//
//    @PutMapping("/hello/{_id}")
//    public String update(@PathVariable String _id, @RequestBody Map<String, String> info, HttpServletResponse response) {
//        Integer id;
//        try {
//            id = Integer.parseInt(_id);
//            if(id>0  && data.get(id)!= null && info.get("value")!= null){
//                data.put(id, info.get("value"));
//                response.setStatus( HttpServletResponse.SC_OK  );
//                return "{\"message\":\"successful\"}";
//            }
//        } catch(Exception e){}
//
//        response.setStatus( HttpServletResponse.SC_BAD_REQUEST );
//        return "{\"message\":\"Data not correct\"}";
//    }
//    @PostMapping(value = "/hello")
//    public String add(@RequestBody Map<String, String> info, HttpServletResponse response){
//        if(info.get("id") != null && info.get("value")!= null) {
//            Integer id;
//            try {
//                id = Integer.parseInt(info.get("id"));
//                if (id > 0 && data.get(id) == null) {
//                    data.put(id, info.get("value"));
//                    response.setStatus(HttpServletResponse.SC_OK);
//                    return "{\"message\":\"successful\"}";
//                }
//            } catch(Exception e){}
//        }
//        response.setStatus( HttpServletResponse.SC_BAD_REQUEST );
//        return "{\"message\":\"Data already correct\"}";
//    }
//    private HashMap<String, Object> convertToJson(HashMap<String,String> _data, String objectname, ArrayList<Object> startdata){//functie die {"data":[{"id":1,"value":"3"}]} bijvoorbeeld doet inplaats van {"3","4"}
//        ArrayList<Object> array = new ArrayList<>();
//        _data.forEach((i, s)->{
//            array.add(new HashMap<String, Object>(){{put("id",i);put("value",s);}});
//        });
//        return new HashMap<String, Object>(){{put("data", array);}};
//    }
//}
