package com.example.demo.controller;

import com.example.demo.models.PersonalInformation;
import com.example.demo.models.WorkExperience;
import com.example.demo.services.PersonalInformationService;
import com.example.demo.services.WorkExperienceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
public class ResumeController {

    @Autowired
    private PersonalInformationService personalInformationservice;
    @Autowired
    private WorkExperienceService workExperienceService;


    @GetMapping("/devide/{number}")
    public String DevideCheck(@PathVariable Integer number){
        if(number % 3 == 0 && number % 5 == 0){
            return "{\"data\":\"FizzBuzz\"}";
        }
        else if(number % 3 == 0){
            return "{\"data\":\"Fizz\"}";
        }
        else if (number % 5 == 0) {
            return "{\"data\":\"Buzz\"}";
        }
        return "{\"data\":\"deviding didn't work\"}";
    }
    @GetMapping("/resume")//voor krijgen van gegevens
    public List<PersonalInformation> getAllData(@RequestParam(required = false) String search) {
        if(search==null){
            return personalInformationservice.getAll();
        }
        else {
            List<PersonalInformation> filterdata = new ArrayList<>();
            personalInformationservice.getAll().forEach((pi)->{
                if(pi.getName().contains(search)){
                    filterdata.add(pi);
                }
            });
            return filterdata;
        }
    }
    @GetMapping("/work")//voor krijgen van gegevens
    public List<WorkExperience> getWorkData(@RequestParam(required = false) String search) {
            return workExperienceService.getAll();
    }
    @GetMapping("/work/add")
    public String test(){
        WorkExperience work = new WorkExperience("205","1","578","1");
        //System.out.println(work);
        workExperienceService.add(work);
        return "{\"data\":\"saved\"}";
    }
}
