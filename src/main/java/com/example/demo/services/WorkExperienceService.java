package com.example.demo.services;


import com.example.demo.models.WorkExperience;
import com.example.demo.repositories.WorkExperienceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WorkExperienceService {
    @Autowired
    private WorkExperienceRepository repository;

    public List<WorkExperience> getAll(){
        return repository.findAll();
    }
    public WorkExperience get(Long id){
        return repository.getOne(id);
    }

    public void add(WorkExperience pi){
        repository.saveAndFlush(pi);
    }
    public void update(WorkExperience pi){
        repository.save(pi);
    }
    public void delete(WorkExperience pi){
        repository.delete(pi);
    }

}
