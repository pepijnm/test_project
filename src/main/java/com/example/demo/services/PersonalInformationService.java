package com.example.demo.services;

import com.example.demo.models.PersonalInformation;
import com.example.demo.repositories.PersonalInformationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonalInformationService {
    @Autowired
    private PersonalInformationRepository repository;

    public List<PersonalInformation> getAll(){
        return repository.findAll();
    }
    public PersonalInformation get(Long id){
        return repository.getOne(id);
    }

    public void add(PersonalInformation pi){
        repository.saveAndFlush(pi);
    }
    public void update(PersonalInformation pi){
        repository.save(pi);
    }
    public void delete(PersonalInformation pi){
        repository.delete(pi);
    }

}
