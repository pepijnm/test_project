package com.example.demo.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class WorkExperience {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String startYear;
    private String startMonth;
    private String endYear;
    private String endMonth;
    public WorkExperience(){}
    public WorkExperience(String _startYear, String _startMonth, String _endYear, String _endMonth){
        this.startYear = _startYear;
        this.startMonth = _startMonth;
        this.endYear = _endYear;
        this. endMonth = _endMonth;
    }

    public String getStartYear() {
        return startYear;
    }

    public String getEndMonth() {
        return endMonth;
    }

    public Long getId() {
        return id;
    }

    public String getStartMonth() {
        return startMonth;
    }

    public String getEndYear() {
        return endYear;
    }
//    @Override
//    public String toString() {
//        return String.format(
//                "Example[id=%d, sy='%m', sm=%a, ey=b, em=c]",
//                id, startYear);
//    }
}
