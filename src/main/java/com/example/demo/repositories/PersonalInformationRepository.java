package com.example.demo.repositories;

import com.example.demo.models.PersonalInformation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface  PersonalInformationRepository extends JpaRepository<PersonalInformation, Long> {

}
